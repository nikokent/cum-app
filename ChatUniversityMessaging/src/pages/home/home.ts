import { Component,ViewChild, AfterViewChecked} from '@angular/core';
import { NavController, Content } from 'ionic-angular';
import { leave } from '@angular/core/src/profile/wtf_impl';
import { Socket } from 'ng-socket-io';
import { Keyboard } from '@ionic-native/keyboard';
import { AbstractControl } from '@angular/forms/src/model';
import { Observable } from 'rxjs/Observable';
import { Inject} from "@angular/core";
import { DOCUMENT } from '@angular/platform-browser';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
@ViewChild(Content) content: Content;
public items: any;
messageInput = "";
  constructor(public navCtrl: NavController, private socket: Socket, private keyboard: Keyboard, @Inject(DOCUMENT) private document: Document) {
    this.setData().subscribe(messages => {
      this.items = messages;
      this.items.forEach(message => {
        if(message.username == window.localStorage.getItem("username")){
          this.addMyMessage(message);
        }
        else{
          this.addMessage(message);
        }
      });
      this.content.scrollToBottom(1000);
      console.log(messages);
    });
    this.newData().subscribe(message => {
      this.items.push(message);
      if(message["username"] == window.localStorage.getItem("username")){
        this.addMyMessage(message);
      }
      else{
        this.addMessage(message);
      }
      this.content.scrollToBottom(1000);
      console.log(message);
    });
  }

  onKeyboardShow(){
  }

  initData(){
    this.socket.emit('refreshData');
  }

  setData(){
    let observable = new Observable(observer => {
      this.socket.on('freshData', (data)=> {
        observer.next(data);
      });
    });
    return observable;
    
  }

  newData(){
    let observable = new Observable(observer => {
      this.socket.on('newMessage', (data) => {
        observer.next(data);
      });
    });
    return observable;
  }

  ionViewDidLoad(){
    this.socket.connect();
    this.initData();
  }

  sendMessage(){
    if(this.messageInput.length > 1){
      var newMsg = this.messageInput;
      var timeStamp = new Date().toISOString().slice(0, 19).replace('T', ' ');
      var dataDisplayed = {
        message: newMsg,
        username: window.localStorage.getItem("username"),
        chat_time: timeStamp
      }
      var data = {
        message: newMsg,
        username: window.localStorage.getItem("username"),
        chat_time: timeStamp
      }
      this.messageInput = "";
      console.log(data);
      this.items.push(dataDisplayed);
      this.addMyMessage(dataDisplayed);
      this.socket.emit('sendmsg',data);
    }
  }

  addMessage(data){
    var list = document.getElementById('container');
    var row = document.createElement('div');
    row.className = "w3-row";
    var card = document.createElement('div');
    card.className = "w3-card-4 w3-animate-left userCard";
    card.style.marginBottom = "8px"; 
    card.style.width= "70%"; 
    card.style.backgroundColor = "white";
    card.style.borderRadius = "5px"; 
    card.style.padding = "1px";
    card.style.alignContent = "left";
    card.style.position = "relative";
    var text = document.createElement('p');
    var name = document.createElement('p');
    var footer = String(data.username) + "       Sent: " + String(data.chat_time).substr(0,10);
    name.innerText = footer;
    name.style.fontSize = "9px";
    name.style.width = "45%";
    name.style.position = "relative";
    name.style.right = "0px";
    name.style.textAlign = "left";
    name.style.color = "white";
    text.innerText = data.message;
    text.style.fontSize = "14px";
    text.style.textAlign = "left";
    text.style.paddingLeft = "7px";
    text.style.paddingRight = "2px";
    text.style.position = "relative";
    text.style.color = "#2c3e50";
    row.style.position = "relative";
    row.style.left = "-5%";
    card.appendChild(text);
    row.appendChild(card);
    row.appendChild(name);
    list.appendChild(row);
  }

  addMyMessage(data){
    var list = document.getElementById('container');
    var row = document.createElement('div');
    row.className = "w3-row";
    var card = document.createElement('div');
    card.className = "w3-card-4 w3-animate-left myCard";
    card.style.marginBottom = "8px"; 
    card.style.width= "70%"; 
    card.style.backgroundColor = "white";
    card.style.borderRadius = "5px"; 
    card.style.padding = "1px";
    card.style.alignContent = "right";
    card.style.position = "relative";
    card.style.cssFloat = "right";
    var text = document.createElement('p');
    var name = document.createElement('p');
    var footer = String(data.username) + "       Sent: " + String(data.chat_time).substr(0,10);
    name.innerText = footer;
    name.style.fontSize = "9px";
    name.style.width = "45%";
    name.style.position = "relative";
    name.style.right = "0px";
    name.style.textAlign = "right";
    name.style.color = "white";
    name.style.cssFloat = "right";
    text.innerText = data.message;
    text.style.fontSize = "14px";
    text.style.textAlign = "right";
    text.style.paddingLeft = "3px";
    text.style.paddingRight = "7px";
    text.style.position = "relative";
    text.style.color = "#f7f1e3";
    row.style.position = "relative";
    row.style.right = "-7%";
    row.style.width = "100%";
    row.style.cssFloat = "right";
    card.appendChild(text);
    row.appendChild(card);
    row.appendChild(name);
    list.appendChild(row);
  }

}
