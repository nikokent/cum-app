import { Component, ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { HomePage } from '../home/home';
import { Socket } from 'ng-socket-io';
import { Keyboard } from '@ionic-native/keyboard';
import { AbstractControl } from '@angular/forms/src/model';
import { Observable } from 'rxjs/Observable';


@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {
  userName="";
  email="";

  constructor(public navCtrl: NavController, private socket: Socket, private keyboard: Keyboard, public navParams: NavParams) {

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad LoginPage');
    this.socket.connect();
    if(window.localStorage.getItem("username") != null){
      this.navCtrl.push(HomePage);
    }
  }

  nextPage(){
    if(this.userName.length > 4){
      var data = {
        username: this.userName,
        email: this.email
      }
      window.localStorage.setItem("username", this.userName);
      window.localStorage.setItem("email", this.email);
      this.socket.emit('newUser', data);
      this.navCtrl.push(HomePage);
    }
    else{
      alert("User Name is too short!");
    }
  }

}
