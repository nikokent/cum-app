webpackJsonp([1],{

/***/ 112:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return LoginPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(43);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__home_home__ = __webpack_require__(164);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_ng_socket_io__ = __webpack_require__(83);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_ng_socket_io___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_ng_socket_io__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ionic_native_keyboard__ = __webpack_require__(88);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var LoginPage = /** @class */ (function () {
    function LoginPage(navCtrl, socket, keyboard, navParams) {
        this.navCtrl = navCtrl;
        this.socket = socket;
        this.keyboard = keyboard;
        this.navParams = navParams;
        this.userName = "";
        this.email = "";
    }
    LoginPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad LoginPage');
        this.socket.connect();
        if (window.localStorage.getItem("username") != null) {
            this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_2__home_home__["a" /* HomePage */]);
        }
    };
    LoginPage.prototype.nextPage = function () {
        if (this.userName.length > 4) {
            var data = {
                username: this.userName,
                email: this.email
            };
            window.localStorage.setItem("username", this.userName);
            window.localStorage.setItem("email", this.email);
            this.socket.emit('newUser', data);
            this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_2__home_home__["a" /* HomePage */]);
        }
        else {
            alert("User Name is too short!");
        }
    };
    LoginPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-login',template:/*ion-inline-start:"/Users/ladmin/Documents/WSU/CPTS322/CUM/ChatUniversityMessaging/src/pages/login/login.html"*/'<!--\n  Generated template for the LoginPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n\n<ion-content class="background">\n    <head>\n      <style>\n        @import url(\'https://fonts.googleapis.com/css?family=Open+Sans:300\');\n      </style>\n    </head>\n    <div id="title1" align = "center">Welcome To</div>\n    <div id="title2" align = "center">Chat University Messaging</div>\n    <ion-card>\n        <ion-card-header id="LoginForm">\n            Login Form\n        </ion-card-header>\n\n      <ion-card-content>\n      <ion-list no-line>\n\n        <ion-item>\n          <ion-input placeholder="Username" [(ngModel)]="userName" type="text" value=""></ion-input>\n        </ion-item>\n      \n        <ion-item>\n          <ion-input placeholder="Email" [(ngModel)]="email" type="text"></ion-input>\n        </ion-item>\n        <button ion-button (click)="nextPage();" block>Login</button>\n\n      </ion-list>\n\n      </ion-card-content>\n      \n  </ion-card>\n\n</ion-content>'/*ion-inline-end:"/Users/ladmin/Documents/WSU/CPTS322/CUM/ChatUniversityMessaging/src/pages/login/login.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* NavController */], __WEBPACK_IMPORTED_MODULE_3_ng_socket_io__["Socket"], __WEBPACK_IMPORTED_MODULE_4__ionic_native_keyboard__["a" /* Keyboard */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* NavParams */]])
    ], LoginPage);
    return LoginPage;
}());

//# sourceMappingURL=login.js.map

/***/ }),

/***/ 122:
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncatched exception popping up in devtools
	return Promise.resolve().then(function() {
		throw new Error("Cannot find module '" + req + "'.");
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = 122;

/***/ }),

/***/ 163:
/***/ (function(module, exports, __webpack_require__) {

var map = {
	"../pages/login/login.module": [
		317,
		0
	]
};
function webpackAsyncContext(req) {
	var ids = map[req];
	if(!ids)
		return Promise.reject(new Error("Cannot find module '" + req + "'."));
	return __webpack_require__.e(ids[1]).then(function() {
		return __webpack_require__(ids[0]);
	});
};
webpackAsyncContext.keys = function webpackAsyncContextKeys() {
	return Object.keys(map);
};
webpackAsyncContext.id = 163;
module.exports = webpackAsyncContext;

/***/ }),

/***/ 164:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return HomePage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(43);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ng_socket_io__ = __webpack_require__(83);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ng_socket_io___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_ng_socket_io__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_native_keyboard__ = __webpack_require__(88);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_rxjs_Observable__ = __webpack_require__(7);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_rxjs_Observable___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_4_rxjs_Observable__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__angular_platform_browser__ = __webpack_require__(32);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};







var HomePage = /** @class */ (function () {
    function HomePage(navCtrl, socket, keyboard, document) {
        var _this = this;
        this.navCtrl = navCtrl;
        this.socket = socket;
        this.keyboard = keyboard;
        this.document = document;
        this.messageInput = "";
        this.setData().subscribe(function (messages) {
            _this.items = messages;
            _this.items.forEach(function (message) {
                if (message.username == window.localStorage.getItem("username")) {
                    _this.addMyMessage(message);
                }
                else {
                    _this.addMessage(message);
                }
            });
            _this.content.scrollToBottom(1000);
            console.log(messages);
        });
        this.newData().subscribe(function (message) {
            _this.items.push(message);
            if (message["username"] == window.localStorage.getItem("username")) {
                _this.addMyMessage(message);
            }
            else {
                _this.addMessage(message);
            }
            _this.content.scrollToBottom(1000);
            console.log(message);
        });
    }
    HomePage.prototype.onKeyboardShow = function () {
    };
    HomePage.prototype.initData = function () {
        this.socket.emit('refreshData');
    };
    HomePage.prototype.setData = function () {
        var _this = this;
        var observable = new __WEBPACK_IMPORTED_MODULE_4_rxjs_Observable__["Observable"](function (observer) {
            _this.socket.on('freshData', function (data) {
                observer.next(data);
            });
        });
        return observable;
    };
    HomePage.prototype.newData = function () {
        var _this = this;
        var observable = new __WEBPACK_IMPORTED_MODULE_4_rxjs_Observable__["Observable"](function (observer) {
            _this.socket.on('newMessage', function (data) {
                observer.next(data);
            });
        });
        return observable;
    };
    HomePage.prototype.ionViewDidLoad = function () {
        this.socket.connect();
        this.initData();
    };
    HomePage.prototype.sendMessage = function () {
        if (this.messageInput.length > 1) {
            var newMsg = this.messageInput;
            var timeStamp = new Date().toISOString().slice(0, 19).replace('T', ' ');
            var dataDisplayed = {
                message: newMsg,
                username: window.localStorage.getItem("username"),
                chat_time: timeStamp
            };
            var data = {
                message: newMsg,
                username: window.localStorage.getItem("username"),
                chat_time: timeStamp
            };
            this.messageInput = "";
            console.log(data);
            this.items.push(dataDisplayed);
            this.addMyMessage(dataDisplayed);
            this.socket.emit('sendmsg', data);
        }
    };
    HomePage.prototype.addMessage = function (data) {
        var list = document.getElementById('container');
        var row = document.createElement('div');
        row.className = "w3-row";
        var card = document.createElement('div');
        card.className = "w3-card-4 w3-animate-left userCard";
        card.style.marginBottom = "8px";
        card.style.width = "70%";
        card.style.backgroundColor = "white";
        card.style.borderRadius = "5px";
        card.style.padding = "1px";
        card.style.alignContent = "left";
        card.style.position = "relative";
        var text = document.createElement('p');
        var name = document.createElement('p');
        var footer = String(data.username) + "       Sent: " + String(data.chat_time).substr(0, 10);
        name.innerText = footer;
        name.style.fontSize = "9px";
        name.style.width = "45%";
        name.style.position = "relative";
        name.style.right = "0px";
        name.style.textAlign = "left";
        name.style.color = "white";
        text.innerText = data.message;
        text.style.fontSize = "14px";
        text.style.textAlign = "left";
        text.style.paddingLeft = "7px";
        text.style.paddingRight = "2px";
        text.style.position = "relative";
        text.style.color = "#2c3e50";
        row.style.position = "relative";
        row.style.left = "-5%";
        card.appendChild(text);
        row.appendChild(card);
        row.appendChild(name);
        list.appendChild(row);
    };
    HomePage.prototype.addMyMessage = function (data) {
        var list = document.getElementById('container');
        var row = document.createElement('div');
        row.className = "w3-row";
        var card = document.createElement('div');
        card.className = "w3-card-4 w3-animate-left myCard";
        card.style.marginBottom = "8px";
        card.style.width = "70%";
        card.style.backgroundColor = "white";
        card.style.borderRadius = "5px";
        card.style.padding = "1px";
        card.style.alignContent = "right";
        card.style.position = "relative";
        card.style.cssFloat = "right";
        var text = document.createElement('p');
        var name = document.createElement('p');
        var footer = String(data.username) + "       Sent: " + String(data.chat_time).substr(0, 10);
        name.innerText = footer;
        name.style.fontSize = "9px";
        name.style.width = "45%";
        name.style.position = "relative";
        name.style.right = "0px";
        name.style.textAlign = "right";
        name.style.color = "white";
        name.style.cssFloat = "right";
        text.innerText = data.message;
        text.style.fontSize = "14px";
        text.style.textAlign = "right";
        text.style.paddingLeft = "3px";
        text.style.paddingRight = "7px";
        text.style.position = "relative";
        text.style.color = "#f7f1e3";
        row.style.position = "relative";
        row.style.right = "-7%";
        row.style.width = "100%";
        row.style.cssFloat = "right";
        card.appendChild(text);
        row.appendChild(card);
        row.appendChild(name);
        list.appendChild(row);
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])(__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* Content */]),
        __metadata("design:type", typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* Content */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* Content */]) === "function" && _a || Object)
    ], HomePage.prototype, "content", void 0);
    HomePage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-home',template:/*ion-inline-start:"/Users/ladmin/Documents/WSU/CPTS322/CUM/ChatUniversityMessaging/src/pages/home/home.html"*/'<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">\n<ion-content class="messageView" [attr.noScroll]="shouldScroll">\n    <div id="container" class="w3-container w3-center w3-margin">\n        <div class="w3-row" id="msg-area">\n            \n        </div>\n    </div>\n<ion-list class="w3-card-4 inputView" inset>\n\n    <ion-item>\n      <ion-label style="padding: 10px">Message:</ion-label>\n      <ion-input id="message-input" [(ngModel)]="messageInput" type="text"></ion-input>\n    </ion-item>\n  \n    <ion-item>\n      <button ion-button (click)="sendMessage()" style="width:90%; left:5%; position:relative; height: 30px;">Send</button>\n    </ion-item>\n  \n  </ion-list>\n\n</ion-content>\n'/*ion-inline-end:"/Users/ladmin/Documents/WSU/CPTS322/CUM/ChatUniversityMessaging/src/pages/home/home.html"*/
        }),
        __param(3, Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Inject"])(__WEBPACK_IMPORTED_MODULE_5__angular_platform_browser__["b" /* DOCUMENT */])),
        __metadata("design:paramtypes", [typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* NavController */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* NavController */]) === "function" && _b || Object, typeof (_c = typeof __WEBPACK_IMPORTED_MODULE_2_ng_socket_io__["Socket"] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2_ng_socket_io__["Socket"]) === "function" && _c || Object, typeof (_d = typeof __WEBPACK_IMPORTED_MODULE_3__ionic_native_keyboard__["a" /* Keyboard */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_3__ionic_native_keyboard__["a" /* Keyboard */]) === "function" && _d || Object, Object])
    ], HomePage);
    return HomePage;
    var _a, _b, _c, _d;
}());

//# sourceMappingURL=home.js.map

/***/ }),

/***/ 220:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser_dynamic__ = __webpack_require__(221);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__app_module__ = __webpack_require__(242);


Object(__WEBPACK_IMPORTED_MODULE_0__angular_platform_browser_dynamic__["a" /* platformBrowserDynamic */])().bootstrapModule(__WEBPACK_IMPORTED_MODULE_1__app_module__["a" /* AppModule */]);
//# sourceMappingURL=main.js.map

/***/ }),

/***/ 242:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__ = __webpack_require__(32);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(43);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_native_splash_screen__ = __webpack_require__(218);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ionic_native_status_bar__ = __webpack_require__(219);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__app_component__ = __webpack_require__(316);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__pages_home_home__ = __webpack_require__(164);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__pages_login_login__ = __webpack_require__(112);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8_ng_socket_io__ = __webpack_require__(83);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8_ng_socket_io___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_8_ng_socket_io__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__ionic_native_keyboard__ = __webpack_require__(88);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};










var config = { url: 'http://ec2-34-233-119-199.compute-1.amazonaws.com:3000', options: {} };
var AppModule = /** @class */ (function () {
    function AppModule() {
    }
    AppModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["NgModule"])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_5__app_component__["a" /* MyApp */],
                __WEBPACK_IMPORTED_MODULE_6__pages_home_home__["a" /* HomePage */],
                __WEBPACK_IMPORTED_MODULE_7__pages_login_login__["a" /* LoginPage */]
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__["a" /* BrowserModule */],
                __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["d" /* IonicModule */].forRoot(__WEBPACK_IMPORTED_MODULE_5__app_component__["a" /* MyApp */], {}, {
                    links: [
                        { loadChildren: '../pages/login/login.module#LoginPageModule', name: 'LoginPage', segment: 'login', priority: 'low', defaultHistory: [] }
                    ]
                }),
                __WEBPACK_IMPORTED_MODULE_8_ng_socket_io__["SocketIoModule"].forRoot(config)
            ],
            bootstrap: [__WEBPACK_IMPORTED_MODULE_2_ionic_angular__["b" /* IonicApp */]],
            entryComponents: [
                __WEBPACK_IMPORTED_MODULE_5__app_component__["a" /* MyApp */],
                __WEBPACK_IMPORTED_MODULE_6__pages_home_home__["a" /* HomePage */],
                __WEBPACK_IMPORTED_MODULE_7__pages_login_login__["a" /* LoginPage */]
            ],
            providers: [
                __WEBPACK_IMPORTED_MODULE_4__ionic_native_status_bar__["a" /* StatusBar */],
                __WEBPACK_IMPORTED_MODULE_3__ionic_native_splash_screen__["a" /* SplashScreen */],
                __WEBPACK_IMPORTED_MODULE_9__ionic_native_keyboard__["a" /* Keyboard */],
                { provide: __WEBPACK_IMPORTED_MODULE_1__angular_core__["ErrorHandler"], useClass: __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["c" /* IonicErrorHandler */] }
            ]
        })
    ], AppModule);
    return AppModule;
}());

//# sourceMappingURL=app.module.js.map

/***/ }),

/***/ 288:
/***/ (function(module, exports) {

/* (ignored) */

/***/ }),

/***/ 316:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MyApp; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(43);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_native_status_bar__ = __webpack_require__(219);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_native_splash_screen__ = __webpack_require__(218);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__pages_login_login__ = __webpack_require__(112);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var MyApp = /** @class */ (function () {
    function MyApp(platform, statusBar, splashScreen) {
        this.rootPage = __WEBPACK_IMPORTED_MODULE_4__pages_login_login__["a" /* LoginPage */];
        platform.ready().then(function () {
            // Okay, so the platform is ready and our plugins are available.
            // Here you can do any higher level native things you might need.
            statusBar.styleDefault();
            splashScreen.hide();
        });
    }
    MyApp = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({template:/*ion-inline-start:"/Users/ladmin/Documents/WSU/CPTS322/CUM/ChatUniversityMessaging/src/app/app.html"*/'<ion-nav [root]="rootPage"></ion-nav>\n'/*ion-inline-end:"/Users/ladmin/Documents/WSU/CPTS322/CUM/ChatUniversityMessaging/src/app/app.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* Platform */], __WEBPACK_IMPORTED_MODULE_2__ionic_native_status_bar__["a" /* StatusBar */], __WEBPACK_IMPORTED_MODULE_3__ionic_native_splash_screen__["a" /* SplashScreen */]])
    ], MyApp);
    return MyApp;
}());

//# sourceMappingURL=app.component.js.map

/***/ })

},[220]);
//# sourceMappingURL=main.js.map