# Chat University Messaging 

This is our prototype chat app for universities to help their students connect through connecting random students at a university by chat.

## Getting Started

You will need Ionic Framework, Node JS, NPM, with NPM our dependencies used: Express, Socket io, and mysql
To install ionic:
-In Terminal run: "sudo npm install -g ionic"

### Installing

git clone git@gitlab.com:nikokent/cum-app.git

## Running the app

To run the app via localhost in browser on port 8100:
-Open the directory “ChatUniversityMessaging” in terminal
-run command “ionic serve”
-open browser with url localhost:8100
