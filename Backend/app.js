//Our dependencies
var express = require('express');
var app = express();
var bodyparser = require('body-parser');
var http = require('http').Server(app);
var io = require('socket.io')(http);
var db = require('./db');

app.use(bodyparser.json());

var port = 3000;
app.route('/').get(function(req,res){
    res.send("Hello There Friend");
});
//dbinfo is our access information for our database. It is currently set
//to public access during testing period but will be later set to only
//communicate with our ec2 instance.
dbinfo = {
    host: 'cumdb.cpionotpegkz.us-east-1.rds.amazonaws.com',
    user: 'cumDB',
    password: 'WashingtonState',
    database: 'CUMdb'
}
//Handling our post requests assuming all json posts lead with an
//'action' key that stores the type of request
app.route('/').post(function(req,res){
    console.log(req.body);
    if (req.body.action == "newUser"){
        delete req.body.action;
        db.insertUser(dbinfo, req.body, function(){
            res.send("New User Added!");
        });
    }
    if (req.body.action == "newMessage"){
        delete req.body.action;
        addMessage(req.body, function(){
            res.send("New Message Added!");
        })
    }
    if(req.body.action == "Messages"){
        db.getMessages(dbinfo,function(){
            res.send(db.messages);
        });
    }
});

//addMessage will be our inserting our message for our db
var addMessage = function(data, completion){
    db.insertMessage(dbinfo, data, function(){
        completion();
    });
};

var addUser = function(data, completion){
    db.insertUser(dbinfo, data, function(){
        console.log(data);
        completion();
    });
};

//messageHistory will temporarily store all the current messages and 
//at the beginning of the server run it will populate with message


//populates messageHistory


//Our listening port
http.listen(port,function(){
    console.log("App has started with port: ", port);
});

//Websockets used to handle realtime messaging
io.sockets.on('connection', function(socket){
    var messageHistory = [];
    console.log("new user connected with id <" + socket.id + '>');
    socket.on('refreshData', function(){
        console.log("REFRESHING!");
        db.getMessages(dbinfo,function(){
            socket.emit('freshData', db.messages);
            console.log(messageHistory);
        });
        
    });
    socket.join('private_room', () => {
        let rooms = Object.keys(socket.rooms);
        console.log(rooms); // [ <socket.id>, 'private_room' ]
        socket.emit('freshData', messageHistory);
    });
    socket.on('sendmsg', function(data){
        console.log('server: %j', data);
        messageHistory.push(data);
        console.log(data);
        addMessage(data, function(){
            socket.broadcast.emit('newMessage', data);
        });
        
    });
    socket.on('newUser', function(data){
        addUser(data, function(){
            socket.emit('userCreated', data);
        });
    });
});